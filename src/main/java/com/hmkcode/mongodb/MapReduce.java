package com.hmkcode.mongodb;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.MongoClient;
import com.mongodb.MapReduceCommand.OutputType;

public class MapReduce {
	
	public static void main(String[] args){
		
		//Connect mongodb
		MongoClient mongo = null;
		try{
			mongo = new MongoClient("localhost",27017);
		}catch(UnknownHostException e){e.printStackTrace();}
		
		//get the database
		//if database doesn't exists, mongodb will create it for you.
		DB db = mongo.getDB("test");
		
		//get the collection
		//if collection doesn't exists, mongodb will create it for you.
		DBCollection collection = db.getCollection("person");
		
		String map = "function (){" +
						"emit('size',{count:1});}";
		
		String reduce = "function(key,values){" +
						"total=0; "+
				        "for(var i in values){" +
						"total += values[i].count;" +
				        "}" +
						" return {count:total}}";
		
		String map2 = "function(){" +
						"var key = this.name;" +						
						"emit(key,this.friends);};";
		
		String reduce2 = "function(key,value) {" +
						"var count=0;" +
				"for(var idx = 0; idx < value.length; idx++){" +
				" count += 1;" +
				"} " +
				"return count};";
		
		
		String map3 = "function() {" +
				"var key = this.name;" +
				"for (var idx = 0; idx < this.friends.length; idx++) {" +
	            "" +
	            "var value = {"+
	                          "count: 1"+
	                          "};"+
	            "emit(key, value);"+
	        "}}";
		
		String reduce3 = "function(keySKU, countObjVals) {" +
            "reducedVal = { count: 0};" +
            "for (var idx = 0; idx < countObjVals.length; idx++) {"+
                "reducedVal.count += countObjVals[idx].count;" +               
            "}" +
            "return reducedVal;"+
         "};";
		
		
		MapReduceCommand cmd = new MapReduceCommand(collection	, map3, reduce3, null, OutputType.INLINE	, null);
		MapReduceOutput out = collection.mapReduce(cmd);
		
		for(DBObject o : out.results()){
			System.out.println(o.toString());
		}
		System.out.println("Done");
		
		
		
		
		
						
		
		
		
	}
	

}
